
# http://razzpisampler.oreilly.com/ch04.html

import RPi.GPIO as GPIO
from time import sleep
from random import randint

pins = [11, 12, 13, 15, 16, 18]

pin_led_states = [
  [-1, -1, -1, -1,  1, 0], # 0
  [-1, -1, -1,  1, -1, 0], # 1
  [-1, -1,  1, -1, -1, 0], # 2
  [-1,  1, -1, -1, -1, 0], # 3
  [ 1, -1, -1, -1, -1, 0], # 4
  
  [-1, -1, -1, -1, 0,  1], # 5
  [-1, -1, -1,  1, 0, -1], # 6
  [-1, -1,  1, -1, 0, -1], # 7
  [-1,  1, -1, -1, 0, -1], # 8
  [ 1, -1, -1, -1, 0, -1], # 9
  
  [-1, -1, -1, 0, -1,  1], # 10
  [-1, -1, -1, 0,  1, -1], # 11
  [-1, -1,  1, 0, -1, -1], # 12
  [-1,  1, -1, 0, -1, -1], # 13
  [ 1, -1, -1, 0, -1, -1], # 14
  
  [-1, -1, 0, -1, -1,  1], # 15
  [-1, -1, 0, -1,  1, -1], # 16
  [-1, -1, 0,  1, -1, -1], # 17
  [-1,  1, 0, -1, -1, -1], # 18
  [ 1, -1, 0, -1, -1, -1], # 19
  
  [-1, 0, -1, -1, -1,  1], # 20
  [-1, 0, -1, -1,  1, -1], # 21
  [-1, 0, -1,  1, -1, -1], # 22
  [-1, 0,  1, -1, -1, -1], # 23
  [ 1, 0, -1, -1, -1, -1], # 24
  
  [0, -1, -1, -1, -1,  1], # 25
  [0, -1, -1, -1,  1, -1], # 26
  [0, -1, -1,  1, -1, -1], # 27
  [0, -1,  1, -1, -1, -1], # 28
  [0,  1, -1, -1, -1, -1], # 29
]

GPIO.setmode(GPIO.BOARD)

def set_pin(pin_index, pin_state):
    if pin_state == -1:
        GPIO.setup(pins[pin_index], GPIO.IN)
    else:
        GPIO.setup(pins[pin_index], GPIO.OUT)
        GPIO.output(pins[pin_index], pin_state)

def light_led(led_number):
    for pin_index, pin_state in enumerate(pin_led_states[led_number]):
        set_pin(pin_index, pin_state)

######

#
def column(n):
    offset = (n - 1) * 5
    
    for led in range(offset, offset + 5):
        light_led(led)
        sleep(0.003)

#
def row(n):
    o = n - 1
    
    for led in [o, o + 5, o + 10, o + 15, o + 20, o + 25]:
        light_led(led)
        sleep(0.003)

#
def select():
    while True:
        input = raw_input("Number (0 to 5):")
        light_led(int(input))

#
def row_seq_for():
    for col in [1, 2, 3, 4, 5]:
        for row in [1, 2, 3, 4, 5, 6]:
            led = (row - 1) * 5 + (col - 1)
            light_led(led)
            sleep(0.1)

#            
def sequencia():
    for led in range(30):
        light_led(led)
        sleep(0.1)

#
def diagonal1():
    lines = [
        [0],
        [1, 5],
        [2, 6, 10],
        [3, 7, 11, 15],
        [4, 8, 12, 16, 20],
        [9, 13, 17, 21, 25],
        [14, 18, 22, 26],
        [19, 23, 27],
        [28, 28],
        [29]
    ]
    
    for line in lines:
        for x in range(10):
            for led in line:
                light_led(led)
                sleep(0.005)

    for line in reversed(lines):
        for x in range(10):
            for led in line:
                light_led(led)
                sleep(0.005)

#
def the_whole_S():
    for led in range(30):
        light_led(led)
        sleep(0.005)

#
def schnecke():
    rango = [0, 5, 10, 15, 20, 25, 26, 27, 28, 29, 24, 19, 14, 9, 4, 3, 2, 1, 6, 11, 16, 21, 22, 23, 18, 13, 8, 7, 12, 17]
    for led in rango:
        light_led(led)
        sleep(0.04)
    for led in reversed(rango):
        light_led(led)
        sleep(0.04)

#
def schnecke2():
    rango = [0, 5, 10, 15, 20, 25, 26, 27, 28, 29, 24, 19, 14, 9, 4, 3, 2, 1, 6, 11, 16, 21, 22, 23, 18, 13, 8, 7, 12, 17, 22, 23, 18, 13, 8, 7, 6, 11, 16, 21, 26, 27, 28, 29, 24, 19, 14, 9, 4, 3, 2, 1]
    for led in rango:
        light_led(led)
        sleep(0.03)
        
# 
def random():
    for x in range(100):
        led = randint(0, 29)
        #print(led);
        light_led(led)
        sleep(0.05)

def smiley():
    for x in range(3000):
        for led in [7, 10, 13, 15, 18, 22]:
            light_led(led)

def d():
    for led in [0, 1, 2, 3, 4, 5, 10, 16, 17, 18, 14, 9]:
        light_led(led)
    
def a():
    for led in [0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 7]:
        light_led(led)
    
def n():
    for led in [0, 1, 2, 3, 4, 6, 12, 18, 20, 21, 22, 23, 24]:
        light_led(led)

def i():
    for led in [0, 4, 5, 6, 7, 8, 9, 10, 14]:
        light_led(led)

def colsss():
    for x in range(2):
        for y in range(20):
            column(1)
            column(2)
            column(3)
        for y in range(20):
            column(4)
            column(5)
            column(6)

def checker():
    for quad in [0, 15, 2, 17]:
        for qq in range(500):
            for row in [0, 1, 2]:
                for col in [0, 1, 2]:
                    led = quad + (row) + (col * 5)
                    light_led(led)

def checkcheck():
    for x in range(400):
        for led in range(0, 28, 2):
            light_led(led)
    for x in range(400):
        for led in range(1, 29, 2):
            light_led(led)
#
def ring_0():
    for led in [0, 1, 2, 3, 4, 5, 9, 10, 14, 15, 19, 20, 24, 25, 26, 27, 28, 29]:
        light_led(led)
        
#
def ring_1():
    for led in [6, 7, 8, 11, 13, 16, 18, 21, 22, 23]:
        light_led(led)

#
def ring_2():
    for led in [12, 17]:
        light_led(led)
        
#
def pegel():
    for hor in range(7):
        for level in range(6):
            for x in range(500):
                for led in range(level):
                    for horC in range(hor):
                        light_led(5 * horC + led)


######################3
## Init pins
for pin in range(5):
    set_pin(pin, -1)


'''
    Main
'''

try:
    while True:
        print("D")
        for x in range(500):
            d()
        
        print("A")
        for x in range(500):
            a()
        
        print("N")
        for x in range(500):
            n()

        print("I\n\n")
        for x in range(500):
            i()
        
        print(":)")
        smiley()
        
        #select()
        
        print("Sequencia")
        sequencia()
            
        print("row_seq_for")
        row_seq_for()

        print("schnecke2")
        for x in range(2):
            schnecke2()
        
        print("Column")
        for cnt in [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1]:
            for y in range(10):
                column(cnt)

        print("Row")
        for cnt in [1, 2, 3, 4, 5, 4, 3, 2, 1]:
            for y in range(10):
                row(cnt)
                
        print("Diagonal")
        for x in range(2):
            diagonal1()

        print("Double Column")
        for x in range(2):
            for cnt in [1, 2, 3, 2]:
                for y in range(10):
                    column(cnt)
                    column(7 - cnt)

        print("Double Row")
        for x in range(2):
            for cnt in [1, 2, 3, 2]:
                for y in range(10):
                    row(cnt)
                    row(6 - cnt)
                    
        print("quadrado")
        for x in range(5):
            for y in range(80):
                ring_0()
            for y in range(100):
                ring_1()
            for y in range(500):
                ring_2()
            for y in range(100):
                ring_1()
                
        print("Checker")
        for x in range(2):
            checker()

        print("Half board")
        colsss()

        print("CheckCheck")
        for x in range(3):
            checkcheck()

        print("Random")
        random()
        
        for x in range(2):
            pegel()
        
        print("The whole show...\n")
        for x in range(20):
            the_whole_S()

except KeyboardInterrupt:
    pass

print("\n\nTHX 4 playing\n")

GPIO.cleanup()
