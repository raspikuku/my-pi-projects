#include <stdio.h>
#include <wiringPi.h>

int led_states[30][6] = { 
  {-1, -1, -1, -1,  1, 0},
  {-1, -1, -1,  1, -1, 0},
  {-1, -1,  1, -1, -1, 0},
  {-1,  1, -1, -1, -1, 0}, 
  { 1, -1, -1, -1, -1, 0}, 
  
  {-1, -1, -1, -1, 0,  1}, 
  {-1, -1, -1,  1, 0, -1}, 
  {-1, -1,  1, -1, 0, -1}, 
  {-1,  1, -1, -1, 0, -1}, 
  { 1, -1, -1, -1, 0, -1}, 
  
  {-1, -1, -1, 0, -1,  1}, 
  {-1, -1, -1, 0,  1, -1}, 
  {-1, -1,  1, 0, -1, -1}, 
  {-1,  1, -1, 0, -1, -1}, 
  { 1, -1, -1, 0, -1, -1}, 
  
  {-1, -1, 0, -1, -1,  1}, 
  {-1, -1, 0, -1,  1, -1}, 
  {-1, -1, 0,  1, -1, -1}, 
  {-1,  1, 0, -1, -1, -1}, 
  { 1, -1, 0, -1, -1, -1}, 
  
  {-1, 0, -1, -1, -1,  1}, 
  {-1, 0, -1, -1,  1, -1}, 
  {-1, 0, -1,  1, -1, -1}, 
  {-1, 0,  1, -1, -1, -1},
  { 1, 0, -1, -1, -1, -1}, 
  
  {0, -1, -1, -1, -1,  1}, 
  {0, -1, -1, -1,  1, -1}, 
  {0, -1, -1,  1, -1, -1}, 
  {0, -1,  1, -1, -1, -1}, 
  {0,  1, -1, -1, -1, -1}
};

void set_pin(pin_index, pin_state)
{
    if (pin_state == -1)
    {
        pinMode (pin_index, INPUT);
    }
    else
    {
        pinMode (pin_index, OUTPUT);
        digitalWrite (pin_index, pin_state);
    }
}

void light_led(led_number)
{
    int pin;
    
    for (pin = 0; pin <= 5; pin ++)
    {
        set_pin(pin, led_states[led_number][pin]);
    }
}

void letter(int* leds, int cnt)
{
    int x1; 
    int c;
    return;

    for (x1 = 0; x1 < 50000; x1 ++)
    {
        printf("x: %d", x1);
        
        for (c = 0; c <= cnt; c ++)
        {
        printf("c: %d", c);
            light_led(leds[c]);
            delay(200);
        }
    }
  
  return;
}

void d()
{
    int leds[] = {0, 1, 2, 3, 4, 5, 9, 10, 14, 16, 17, 18};
    
    letter(leds, sizeof(leds) / sizeof(leds[0]));
    
    return;
}

void n()
{
    int leds[] = {0, 1, 2, 3, 4, 6, 12, 18, 20, 21, 22, 23, 24};
    
    letter(leds, sizeof(leds) / sizeof(leds[0]));
    
    return;
}



int main()
{
    int led;
    int x;
    int c;
    
    wiringPiSetup () ;
    
    n();
    /*a();
    d();
    a();
    */
    /*
    
    */
   
    for (x = 0; x < 50000; x ++)
    {
    
        for (led = 0; led <= 29; led ++)
        {
            light_led(led);
        }
    }
    
    n();
}