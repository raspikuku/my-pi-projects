#!/usr/bin/env python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO

led_pin = 18

GPIO.setmode(GPIO.BCM)

GPIO.setup(led_pin, GPIO.OUT)

pwm_led = GPIO.PWM(led_pin, 500)
pwm_led.start(50)

try:
    while True:
        input = raw_input("Enter Brightness (0 to 100):")

        duty = int(input)

        pwm_led.ChangeDutyCycle(duty)

except KeyboardInterrupt:
    pass

pwm_led.stop()

GPIO.cleanup()
