import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

p = GPIO.PWM(12, 700)  # channel=12 frequency=50Hz
p.start(50)

try:
    while True:
        
        for freq in range(200, 800, 5):
            p.ChangeFrequency(freq)
            time.sleep(0.001)
            
        for freq in range(800, 200, -5):
            p.ChangeFrequency(freq)
            time.sleep(0.001)
            
except KeyboardInterrupt:
    pass

p.stop()

GPIO.cleanup()