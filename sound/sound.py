import RPi.GPIO as GPIO

spkr_pin = 18

GPIO.setmode(GPIO.BCM)

GPIO.setup(spkr_pin, GPIO.OUT)

pwm_spkr = GPIO.PWM(spkr_pin, 150)
pwm_spkr.start(50)

try:
    while True:
        input = raw_input("Enter Frequency (1 to 10000):")

        freq = int(input)

        pwm_spkr.ChangeFrequency(freq) 

except KeyboardInterrupt:
    pass

pwm_spkr.stop()

GPIO.cleanup()
