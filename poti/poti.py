# http://learn.adafruit.com/basic-resistor-sensor-reading-on-raspberry-pi/basic-photocell-reading

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

a_pin = 18
b_pin = 23

pin_LED = 24
GPIO.setup(pin_LED, GPIO.OUT)

def discharge():
    GPIO.setup(a_pin, GPIO.IN)
	GPIO.setup(b_pin, GPIO.OUT)
	GPIO.output(b_pin, False)
	time.sleep(0.005)

def charge_time():
	GPIO.setup(b_pin,GPIO.IN)
	GPIO.setup(a_pin, GPIO.OUT)
	count = 0
	GPIO.output(a_pin, True)
	while not GPIO.input(b_pin):
		count = count + 1
	return count

def analog_read():
	discharge()
	return charge_time()

while True:
	v = analog_read()
	print(v)
	
	if v > 1000:
		GPIO.output(pin_LED, True)
		print("heyoo")
	else:
		GPIO.output(pin_LED, False)

	time.sleep(1)

