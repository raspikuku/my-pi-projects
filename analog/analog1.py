#!/usr/bin/env python

# http://learn.adafruit.com/basic-resistor-sensor-reading-on-raspberry-pi/basic-photocell-reading
 
# Example for RC timing reading for Raspberry Pi
# Must be used with GPIO 0.3.1a or later - earlier verions
# are not fast enough!
 
import RPi.GPIO as GPIO, time, os      
 
GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.OUT)
 
def RCtime (RCpin):
        reading = 0
        GPIO.setup(RCpin, GPIO.OUT)
        GPIO.output(RCpin, GPIO.LOW)
        time.sleep(0.1)
 
        GPIO.setup(RCpin, GPIO.IN)
        # This takes about 1 millisecond per loop cycle
        while (GPIO.input(RCpin) == GPIO.LOW):
                reading += 1
        return reading
 
try:
    while True:
        valor = RCtime(18)     # Read RC timing using pin #18
        print valor# / 4
        if valor > 1000:
            print("Se prende la luz")
            GPIO.output(25, GPIO.HIGH)
        else:
            GPIO.output(25, GPIO.LOW)
        time.sleep(3)
except KeyboardInterrupt:
    pass

GPIO.cleanup()
