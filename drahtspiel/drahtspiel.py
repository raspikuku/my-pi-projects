#!/usr/bin/env python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BOARD)

GPIO.setup(7, GPIO.IN)
GPIO.setup(11, GPIO.IN)
GPIO.setup(13, GPIO.IN)

status = -1

def playSound(name):
  os.system('mpg321 ' + name +'.mp3 > /dev/null 2>&1')

try:

    while True:
        if GPIO.input(7):
             #print "button 1 pressed"
             print "Los"
             status = 0

        if GPIO.input(11):
             #print "button 2 pressed"
             if status == 0:
               print "BOOOOO"
               playSound('booo')
               status = 1

        if GPIO.input(13):
             #print "button 3 pressed"
             if status == 0:
               print "Finished !"
               status = 2
             elif status == 1:
               print "Zurück zum Start..."

        time.sleep(0.1)

except KeyboardInterrupt:
    pass

GPIO.cleanup()
